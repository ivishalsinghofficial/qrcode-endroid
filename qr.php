<?php

header('Content-Type: image/png');
require 'vendor/autoload.php';

use Endroid\QrCode\QrCode;


$in = strval($_POST['myinput']);
$logo = strval($_POST['logo']);


$qrcode = new QrCode($in);
$qrcode->setLogoPath($logo);
$qrcode->setLogoSize(50);
$qrcode->setSize(250);
echo $qrcode->writeString();
die();

?>


/*
$qrCode = new QrCode();
$qrCode->setText('Wisemonkeys')
->setSize(300)
->setErrorCorrection('high')
->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
// Path to your logo with transparency
->setLogo("favicon.png")
// Set the size of your logo, default is 48
->setImageType(QrCode::IMAGE_TYPE_PNG)
;

header('Content-Type: '.$qrCode->getContentType());
$qrCode->render();

echo $qrCode->writeString();
?>
*/
